<?php


namespace App\Services;

use App\Repositories\CompanyRepository;
use App\Services\BaseService;

class CompanyService extends BaseService
{
    public function __construct(CompanyRepository $repo)
    {
        $this->repo = $repo;
    }
}
