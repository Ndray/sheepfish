<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\Tender;

class FetchTenders extends Command
{
    protected $signature = 'fetch:tenders';
    protected $description = 'Fetch latest tenders from the API';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $response = Http::get('https://public.api.openprocurement.org/api/0/tenders?descending=1');
        $tenders = $response->json()['data'];

        foreach ($tenders as $tender) {
            $tenderResponse = Http::get('https://public.api.openprocurement.org/api/0/tenders/' . $tender['id']);
            $tenderData = $tenderResponse->json()['data'];

            Tender::updateOrCreate(
                ['tender_id' => $tenderData['id']],
                [
                    'description' => $tenderData['description'] ?? 'No description available',
                    'amount' => $tenderData['value']['amount'] ?? 0,
                    'date_modified' => $tenderData['dateModified'] ?? now()
                ]
            );
        }

        $this->info('Tenders fetched successfully.');
    }
}
