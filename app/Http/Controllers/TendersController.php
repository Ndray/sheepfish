<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tender;

class TendersController extends Controller
{
    public function index()
    {
        $tenders = Tender::orderBy('date_modified', 'desc')->take(10)->get();
        $totalAmount = $tenders->sum('amount');

        return view('tenders.index', compact('tenders', 'totalAmount'));
    }}
