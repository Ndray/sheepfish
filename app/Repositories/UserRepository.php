<?php


namespace App\Repositories;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use Illuminate\Support\Collection;


class UserRepository extends BaseRepository
{
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function activeUsersCount(): int
    {
        return $this->model->where("type", "user")->where("status", "active")->count();
    }

    public function getNotAdminUsers(): Collection
    {
        return $this->model->where("type", "<>", "admin")->get();
    }

}

