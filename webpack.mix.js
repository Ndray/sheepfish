const mix = require('laravel-mix');

mix.js('resources/js/app.js', 'public/js')  // компілює JS з resources/js/app.js в public/js/app.js
    .sass('resources/sass/app.scss', 'public/css'); // компілює SCSS з resources/sass/app.scss в public/css/app.css

if (mix.inProduction()) {
    mix.version(); // активує версіонування ресурсів у продакшні
}
