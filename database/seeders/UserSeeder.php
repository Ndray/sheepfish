<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::factory()->create([
            'email' =>fake()->unique()->safeEmail(),
            'name' => 'demo',
            'password' => 'demo',
        ]);

        User::factory()->times(1)->create();
    }

}
