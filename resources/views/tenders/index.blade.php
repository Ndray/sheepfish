<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tenders</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
<div class="container mt-5">
    <h1>Tenders List</h1>
    <p>Загальна сума: {{ $totalAmount }}</p>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th scope="col">Tender ID</th>
            <th scope="col">Description</th>
            <th scope="col">Amount</th>
            <th scope="col">Date Modified</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($tenders as $tender)
            <tr>
                <td>{{ $tender->tender_id }}</td>
                <td>{{ $tender->description }}</td>
                <td>{{ $tender->amount }}</td>
                <td>{{ $tender->date_modified }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
